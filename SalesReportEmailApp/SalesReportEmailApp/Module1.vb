﻿Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Configuration

Module Module1

    Sub Main()
        Dim connString As String = ConfigurationSettings.AppSettings("BLYNKCOREAPPSQLConnectionString")
        Dim app As String = ConfigurationSettings.AppSettings("App")
        Dim emailToConfig As String = ConfigurationSettings.AppSettings("EmailTo")

        'Dim _sql As String = "select Sum(ISNULL(paymentAmount,0)) as totalsales from SF_OrderPayment a inner join [qf_session] b on a.cashiersessionid=b.id where isvoid=0 AND b.shiftid LIKE '" & shiftID & "' and parentid in (select ID from [Order] WHERE posterminalid='" & posNumber & "' and App LIKE '" & app & "' and grandtotal > 0 and orderstatus<>'Cancelled')"
        Dim _sqlPayment As String = String.Format("select Sum(ISNULL(a.paymentAmount,0)) as total from SF_OrderPayment a inner join [qf_session] b on a.cashiersessionid=b.id inner join [Order] c on a.ParentID = c.ID where (a.isTip <> 1 or a.isTip is null) and isvoid=0 and cast(c.DateCreated as date) = cast(getdate() as date) and c.App = '{0}' and c.orderStatus <> 'Cancelled'", app)
        Dim _sqlRefund As String = String.Format("select Sum(ISNULL(a.RefundAmount,0)) as total from SF_OrderRefund a inner join [qf_session] b on a.cashiersessionid=b.id inner join [Order] c on a.ParentID = c.ID where cast(c.DateCreated as date) = cast(getdate() as date) and c.App = '{0}' ", app)

        Dim dtPayment As DataTable = ExecuteSQLDatatable(_sqlPayment, connString)
        Dim dtRefund As DataTable = ExecuteSQLDatatable(_sqlRefund, connString)
        If dtPayment.Rows.Count > 0 And dtRefund.Rows.Count > 0 Then
            Dim itemPayment As DataRow = dtPayment.Rows(0)
            Dim itemRefund As DataRow = dtRefund.Rows(0)
            Dim payment As Decimal = 0
            Dim refund As Decimal = 0
            Decimal.TryParse(itemPayment("total").ToString(), payment)
            Decimal.TryParse(itemRefund("total").ToString(), refund)
            Dim smtpAddress As String = ConfigurationSettings.AppSettings("smtpAddress")
            Dim portNumber As Integer = Convert.ToInt32(ConfigurationSettings.AppSettings("portNumber"))
            Dim enableSSL As Boolean = False

            Dim emailFrom As String = ConfigurationSettings.AppSettings("emailFrom")
            Dim displayName As String = ConfigurationSettings.AppSettings("displayName")
            Dim password As String = ConfigurationSettings.AppSettings("password")
            Dim emailTo As String = emailToConfig
            Dim subject As String = "Sales Report - " & DateTime.Now.ToString("dd/MM/yy")
            Dim body As String = String.Format("BlynkPOS Sales For {0}:<br>=================================<br>Gross Sales    : $ {1}<br>Total Refunds : $ {2}<br>NET  Sales     :  $ {3}", DateTime.Now.ToString("dd/MM/yy hh:mm"), payment.ToString("N"), refund.ToString("N"), (payment - refund).ToString("N"))

            Using mail As New MailMessage()
                mail.From = New MailAddress(emailFrom, displayName)
                mail.[To].Add(emailTo)
                mail.Subject = subject
                mail.Body = body
                mail.IsBodyHtml = True

                Using smtp As New SmtpClient(smtpAddress, portNumber)
                    smtp.Credentials = New Net.NetworkCredential(emailFrom, password)
                    smtp.EnableSsl = enableSSL
                    smtp.Send(mail)
                End Using
            End Using
        End If
    End Sub

    Private Function ExecuteSQLDatatable(query As String, conn As String) As DataTable
        Using connection As New SqlConnection(conn)
            Dim result As New DataTable()
            Try
                connection.Open()
                Dim command As New SqlCommand(query, connection)
                Dim ds As New DataSet()
                Dim da As New SqlDataAdapter(command)
                da.Fill(ds)

                result = ds.Tables(0)
            Catch ex As Exception
            Finally
                connection.Close()
                connection.Dispose()
            End Try
            Return result
        End Using
    End Function

End Module
